[Deprecated] Generated proto and gRPC classes for Google Cloud Platform in Go
================================================================

This repository is deprecated. Please use https://github.com/google/go-genproto instead.

This repository contains the Go classes generated from protos contained in
[Google APIs][].

[gRPC]: http://grpc.io
[Google APIs]: https://github.com/googleapis/googleapis/
